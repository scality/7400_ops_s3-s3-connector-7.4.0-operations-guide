﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd">
    <head>
    </head>
    <body>
        <h1 MadCap:autonum="1. &#160;">Multi-Site Deployments</h1>
        <p>The S3 Connector includes two multi-site deployment models:</p>
        <ul>
            <li>Geo-stretched Deployment</li>
            <li>Asynchronous Geo-replication</li>
        </ul>
        <h2 MadCap:autonum="1.1. &#160;">Geo-Stretched Deployment</h2>
        <p>In a stretched deployment model, the physical machines used to host the data RING and  S3 Connector containers are physically deployed across the two or three sites. By having a single stretched data RING, this model brings an advantage over traditional replication by managing a single logical copy of the data, which can reduce the recovery point objective. For more information see “Multi-Site Deployments” in the <span class="PublicationName"><MadCap:variable name="General.SetupInstall" /></span>. </p>
        <p>This chapter provides data center failover procedures for the following multi-site deployments:</p>
        <ul>
            <li><a href="#Three-Si">Three-site, twelve-server deployment</a>
            </li>
            <li><a href="#Two-Site">Two-site, twelve-server deployment</a>
            </li>
            <li><a href="#Three-Si2">Three-site, six-server deployment</a>
            </li>
            <li><a href="#Two-Site2">Two-site, six-server deployment</a>
            </li>
        </ul>
        <h3 MadCap:autonum="1.1.1 &#160;"><a name="Three-Si"></a>Three-Site, Twelve-Server Deployment</h3>
        <p style="text-align: left;">In the case of three sites with twelve or more servers, the S3 Connector stretched model provides continued service availability if any one of the three data centers suffers a short or extended outage, or even if it fails permanently.</p>
        <p>The deployment configuration with warm standby (WSB) metadata servers ensures that at least:</p>
        <ul>
            <li style="text-align: left;page-break-inside: avoid;">Three  active metadata servers are available after any site failure, which is the minimum needed to continue service and data handling operations. This is considered a degraded mode, because no additional active metadata servers can fail while the service remains available.</li>
            <li style="text-align: left;">Two or more WSBs are available after any site failure, and these can be activated to restore the full five  active metadata servers and reestablish nominal operations (so the system is no longer in degraded mode). </li>
        </ul>
        <p style="page-break-after: avoid;column-break-after: avoid;frame-break-after: avoid;text-align: left;">In the event of any one data center failure:</p>
        <ol>
            <li style="page-break-before: avoid;">The system and data remain available from the surviving two sites. The system automatically fails over operations to the remaining active metadata servers. This automated failover occurs in seconds (typically less than five), including failures requiring election of a new Metadata leader.
            <p class="BoxNote">This may cause a small number of error code returns (http 500s) to applications making service requests during this time. These requests may be retried and will be serviced successfully after the metadata failover completes.</p></li>
            <li>
                <p style="text-align: left;">As the system is now running in degraded mode with fewer than five active metadata servers, the system administrator may now activate the WSBs. This will restore the system from degraded mode and reinstate nominal operations with the full complement of five active metadata servers. </p>
                <p class="BoxNote">This is accomplished by running the Ansible deployment <span class="FileName">run.yml</span> playbook with an additional command line option. See <MadCap:xref href="../Site Recovery Procedures.htm">“Site Recovery Procedures” on page&#160;1</MadCap:xref> for details.</p>
            </li>
        </ol>
        <p style="text-align: left;">In the case of two simultaneous data center outages or failures, the S3 Connector service is stopped. Data is still protected in the RING unless the failed sites have been destroyed permanently. Please contact Scality Customer Support for the operations needed to restore service.</p>
        <h3 MadCap:autonum="1.1.2 &#160;"><a name="Two-Site"></a>Two-Site, Twelve-Server Deployment</h3>
        <p style="text-align: left;">In the case of two sites with twelve or more servers, the S3 Connector stretched model provides administrator-driven, non-automated failover if either site suffers a short or extended outage, or even fails permanently. The deployment configuration with five active and five WSB metadata servers provides the following failover properties: </p>
        <ul>
            <li><b>Minority site failure</b>—Three active metadata servers remain on the majority site, but  the service is halted because the metadata leader was on the failed minority site, and leader election is still enforced there. To continue the service, the administrator must invoke a change to the leader election to the majority site.<ul><li>The change in leader election can be modified directly in the configuration files, or given as a command line argument: <code>-e env_metadata_force_leader_on_site=majority_site</code> for the Ansible run playbook.</li><li>The service then elects a new leader on the majority site and continues operations. This election process may result in a short (&lt; 5 sec) service disruption. During this disruption error code (http 500) may be returned to application requests. These requests may be retried by the application and will be serviced successfully after the metadata failover completes. Because data and metadata updates were configured across both sites, the system maintains full consistency and the Recovery Point Objective (RPO) is therefore zero.</li><li>The Recovery Time Objective (RTO) will be minutes to hours depending largely on the administrator initiating the leader election change.</li></ul></li>
            <li style="text-align: left;"><b>Majority site failure</b>—Because two active metadata servers remain on the minority site after a majority site failure, the S3 Connector service is halted. The administrator can initiate a recovery on the minority site to activate the WSB servers and restore nominal service with five active metadata servers and two failure tolerance.<ul><li>Because the metadata leaders were already on the minority site, the site is consistent and the RPO is zero. The recovery procedure to activate the minority site by bringing the WSBs online is administrator initiated, and will run for several minutes. Typically this means the RTO is minutes to hours, largely depending on the time for the administrator to start the site recovery.</li></ul></li>
            <li style="text-align: left;"><b>For recovery</b>—Run the Ansible deployment run.yml playbook with an additional command line option. See<MadCap:xref href="../Site Recovery Procedures.htm">“Site Recovery Procedures” on page&#160;1</MadCap:xref> for details.</li>
        </ul>
        <h3 MadCap:autonum="1.1.3 &#160;"><a name="Three-Si2"></a>Three-Site, Six-Server Deployment</h3>
        <p style="text-align: left;">In the case of three sites with six servers, the S3 Connector stretched model provides automated continued service availability in the event any one of the three data centers suffers a short or extended outage, or even fails permanently.</p>
        <p style="text-align: left;">In event of site failure, the system administrator may activate the WSB server. It enforces the system and allows it to lose one more server.</p>
        <h3 MadCap:autonum="1.1.4 &#160;"><a name="Two-Site2"></a>Two-Site Six-Server Deployment</h3>
        <p style="text-align: left;">In the case of two sites with six servers, the S3 Connector stretched model provides administrator-driven, non-automated, failover if either site suffers a short or extended outage, or even fails permanently.</p>
        <p style="text-align: left;">The procedure to switch from one site to another is the same as that described above for the two-site deployment with twelve servers. The quorum is maintained, since there are three servers on each site and a new leader can be elected on each site.</p>
        <p class="BoxIMPORTANT">Unlike the twelve-server deployment, the six-server deployment cannot support additional failures, because it is unable to lose one more node with only one site to obtain a quorum.</p>
        <h2 MadCap:autonum="1.2. &#160;">Asynchronous Geo-Replication</h2>
        <p>In two-site asynchronous replication, S3 Connector provides asynchronous replication of objects in S3 buckets set up for replication. This can be used as a disaster recovery solution in which the target site can be used when the source/main site suffers an outage. This failover to the target during an outage is not automated:&#160;an administrator carries out the failover manually, redirecting the customer’s application to the endpoints on the target and using a new pair of credentials (access key and secret key) generated at the target site.</p>
        <p class="BoxNote">Because Vault is not replicated, the source and target credentials are not the same.</p>
    </body>
</html>