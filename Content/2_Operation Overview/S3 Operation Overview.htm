<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd">
    <head>
        <link href="../Resources/TableStyles/SimpleDefinitions.css" rel="stylesheet" MadCap:stylesheetType="table" />
        <link href="../Resources/TableStyles/DetailedwithPadding.css" rel="stylesheet" MadCap:stylesheetType="table" />
    </head>
    <body>
        
        <h1 MadCap:autonum="1. &#160;">S3 Connector Overview</h1>
        <p>This section describes the layout of configuration files for S3 Connector and discusses the structure and the file heirarchies and operating details of <MadCap:variable name="General.ProductNameShort" />’s Metadata service component.</p>
        <h2 MadCap:autonum="1.1. &#160;">Architecture</h2>
        <p>S3 Connector Metadata (bucket) multi-disk storage uses disks much as RAID0 does, aggregating them into one large logical volume to use all available storage space. In this setup, from the Metadata service’s point of view, losing either disk means  losing the server.</p>
        <p>One mount point/disk remains necessary for most  containers, including Vault storage, but this can also be included in the set of mount points/disks provided to Metadata for bucket storage.</p>
        <h2 MadCap:autonum="1.2. &#160;">S3 Connector Operations with Federation</h2>
        <p>Using the Federation folder during the installation facilitates S3 Connector operations. Using this tool to operate an S3 Connector cluster is strongly advised.</p>
        <p>The Federation folder can be stored anywhere in the Supervisor. In a fresh 7.4 installation, this is  <span class="ElementName">/srv/scality/s3/s3-offline</span>.</p>
        <p>The Federation folder contains the following Ansible resources:</p>
        <ul>
            <li>The ansible-playbook wrapper shell script used by the offline installer</li>
            <li>The ansible.cfg local configuration</li>
            <li>The roles/ folder containing the Ansible roles that automate S3 component configuration. Do not modify the files in this folder except as recommended by Scality.</li>
            <li><span class="ElementName">env/&lt;my-env&gt;/</span>:  After a fresh 7.4 install, <code>&lt;my-env&gt;</code> is set to “s3config”  but this can be arbitrarily renamed.</li>
            <li><span class="ElementName">env/&lt;my-env&gt;/group_vars/all</span>: Contains Ansible variables that set the roles’ behavior</li>
            <li>The env/&lt;my-env&gt;/inventory file; it lists the target servers used for the deployment and indicating where the various component services are to be hosted,</li>
            <li>The run.yml ansible playbook that orchestrates the run of the roles/run-* roles on the servers hosting the S3 components;  you shall never modify this file unless you’re told so by Scality staff,</li>
            <li>The tooling-playbooks/ folder in which you will find various ansible playbooks and roles that can help operating the S3 cluster.</li>
        </ul>
        <p style="page-break-after: avoid;">The offline installer provides a Python virtualenv containing Ansible. You can add it to your PATH by activating this virtualenv:</p><pre xml:space="preserve">$ source ${S3_OFFLINE_DIR}/repo/venv/bin/activate
$ ansible -i [...]/env/&lt;my-env&gt;/inventory -m [...] -a [...]</pre>
        <p class="BoxNote" style="text-align: left;">If S3 was installed from a RING installer, <code>S3_OFFLINE_DIR=/srv/scality/s3/s3-offline</code></p>
        <p style="page-break-after: avoid;">The run.yml playbook run can be limited to:</p>
        <ul>
            <li>A certain amount of hosts, using the <code>--limit</code> option. See <a href="http://docs.ansible.com/ansible/latest/intro_patterns.html">http://docs.ansible.com/ansible/latest/intro_patterns.html</a> .</li>
            <li>Pre-determined tags with the <code>--tags</code> option. See <MadCap:xref href="../4_Configuring Vault (IAM Management)/Configuring S3 Vault (IAM management).htm#Configur">“Configuring S3 Vault (IAM Management)” on page&#160;1</MadCap:xref>.</li>
        </ul>
        <p class="BoxIMPORTANT">Under some circumstances, for example when a variable has been modified in env/&lt;my-env&gt;/group_vars/all, running run.yml without the <code>--limit</code> option can lead to a few minutes of production outage.</p>
        <p>The Ansible roles:</p>
        <ul>
            <li>Prepare the Docker environment (packages) for the Red Hat operating systems</li>
            <li>Prepare the directories and configuration files in the hosts registered in the inventory file</li>
            <li>Spawn Docker containers</li>
            <li>Ensure that binaries run correctly</li>
        </ul>
        <h2 MadCap:autonum="1.3. &#160;">Docker in the S3 Connector Environment</h2>
        <p>S3 Connector uses Docker to manage binary deployment within the S3 infrastructure.</p>
        <p>Binaries are shipped within Docker images, and are run on Docker containers</p>
        <p>Particular care has been taken to prevent binaries from reading or writing to files inside the Docker container. This is why Docker containers mount the following Docker volumes:</p>
        <ul>
            <li><span class="ProprietaryFileName">/conf</span> for configuration files</li>
            <li><span class="ProprietaryFileName">/data</span> (or /databases) for stateful data</li>
            <li><span class="ProprietaryFileName">/logs</span> for log files</li>
        </ul>
        <p>Each of these volumes points to a folder stored in the host’s file system. See the following section for a mapping of these directories. You can verify where the volumes are stored with the command: </p><pre xml:space="preserve">$ docker inspect &lt;container-name&gt;</pre>
        <p>See <MadCap:xref href="../9_Managing Containers/Managing Containers.htm#Managing">“Managing Containers” on page&#160;1</MadCap:xref>  for more on Docker containers.</p>
        <h2 MadCap:autonum="1.4. &#160;">S3 Connector File Layout</h2>
        <p>In previous S3 Connector versions, the data stored by any component was  located under the mount point specified in the Ansible variable <span class="Code_Terminal">env_host_data</span> in <span class="ProprietaryFileName">env/ENV_NAME/group_vars/all</span>.</p>
        <p style="page-break-after: avoid;">In S3 Connector, variables specify the types of mount points:</p>
        <table style="width: 100%;mc-table-style: url('../Resources/TableStyles/SimpleDefinitions.css');" class="TableStyle-SimpleDefinitions" cellspacing="0">
            <col class="TableStyle-SimpleDefinitions-Column-Column1" style="width: 190px;" />
            <col class="TableStyle-SimpleDefinitions-Column-Column1" style="width: 398px;" />
            <tbody>
                <tr class="TableStyle-SimpleDefinitions-Body-Row1">
                    <td class="TableStyle-SimpleDefinitions-BodyE-Column1-Row1" style="text-align: center;">
                        <p style="text-align: center;"><b>env_host_conf</b>
                        </p>
                    </td>
                    <td class="TableStyle-SimpleDefinitions-BodyD-Column1-Row1" style="text-align: center;">
                        <p style="text-align: center;">Base path for storing the configuration files the installer generates</p>
                    </td>
                </tr>
                <tr class="TableStyle-SimpleDefinitions-Body-Row1">
                    <td class="TableStyle-SimpleDefinitions-BodyE-Column1-Row1">
                        <p style="text-align: center;"><b>env_host_data</b>
                        </p>
                    </td>
                    <td class="TableStyle-SimpleDefinitions-BodyD-Column1-Row1">
                        <p style="text-align: center;">Base path for storing data handled by the S3 Connector components</p>
                    </td>
                </tr>
                <tr class="TableStyle-SimpleDefinitions-Body-Row1">
                    <td class="TableStyle-SimpleDefinitions-BodyE-Column1-Row1" style="text-align: center;">
                        <p style="text-align: center;"><b>env_host_logs</b>
                        </p>
                    </td>
                    <td class="TableStyle-SimpleDefinitions-BodyD-Column1-Row1" style="text-align: center;">
                        <p style="text-align: center;">Base path for storing the S3 Connector component’s logs</p>
                    </td>
                </tr>
                <tr class="TableStyle-SimpleDefinitions-Body-Row1">
                    <td class="TableStyle-SimpleDefinitions-BodyB-Column1-Row1" style="text-align: center;">
                        <p style="text-align: center;"><b>env_metadata_bucket_datapaths</b>
                        </p>
                    </td>
                    <td class="TableStyle-SimpleDefinitions-BodyA-Column1-Row1" style="text-align: center;">
                        <p style="text-align: center;">List of mount points for Metadata-bucket to use for storing the bucket's metadata</p>
                    </td>
                </tr>
            </tbody>
        </table>
        <p>These folders contain per-component folders that house associated data for one or more containers. For example, <span class="ProprietaryFileName">group_vars/all</span> might contain (among other settings) the following:</p><pre>env_host_conf: /etc/scality-s3connector</pre><pre>env_host_logs: /var/logs/scality-s3connector</pre><pre>env_host_data: /scality/s3/data</pre>
        <p>Using the same example, the Metadata bucket storage component reveals the following paths on any machine hosting the metadata-bucket component:</p><pre>/etc/scality-s3connector/scality-metadata-bucket/conf/</pre><pre>/var/logs/scality-s3connector/scality-metadata-bucket/logs/</pre><pre>/scality/s3/data/scality-metadata-bucket/databases</pre>
        <p>Most components show similar paths, although the Metadata bucket storage has some specificities. The part of the path containing the component’s name—the <i>component-specific basename</i>—changes the most.</p>
        <h2 MadCap:autonum="1.5. &#160;">Metadata Component Specifics</h2>
        <h3 MadCap:autonum="1.5.1 &#160;">Metadata Flavors</h3>
        <p>The Metadata component exists in two flavors: <i>Bucket</i> and <i>Vault</i>. </p>
        <table style="width: 100%;mc-table-style: url('../Resources/TableStyles/DetailedwithPadding.css');margin-left: auto;margin-right: auto;" class="TableStyle-DetailedwithPadding" cellspacing="0">
            <col style="width: 50pt;" class="TableStyle-DetailedwithPadding-Column-Column1" />
            <col class="TableStyle-DetailedwithPadding-Column-Column1" />
            <col class="TableStyle-DetailedwithPadding-Column-Column1" />
            <thead>
                <tr class="TableStyle-DetailedwithPadding-Head-Header1">
                    <th class="TableStyle-DetailedwithPadding-HeadD-Column1-Header1" colspan="3">
                        Metadata Layer Servicing
                    </th>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Head-Header1">
                    <td class="TableStyle-DetailedwithPadding-HeadE-Column1-Header1">
                        Flavor
                    </td>
                    <td class="TableStyle-DetailedwithPadding-HeadE-Column1-Header1">
                        Complemented Component
                    </td>
                    <td class="TableStyle-DetailedwithPadding-HeadD-Column1-Header1">
                        Serviced Container
                    </td>
                </tr>
            </thead>
            <tbody>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">Bucket</td>
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">Metadata
 (Bucketd)</td>
                    <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">scality-bucketd</td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td class="TableStyle-DetailedwithPadding-BodyB-Column1-Body1">Vault</td>
                    <td class="TableStyle-DetailedwithPadding-BodyB-Column1-Body1">Vault</td>
                    <td class="TableStyle-DetailedwithPadding-BodyA-Column1-Body1">scality-vault</td>
                </tr>
            </tbody>
        </table>
        <table style="width: 100%;mc-table-style: url('../Resources/TableStyles/DetailedwithPadding.css');margin-left: auto;margin-right: auto;" class="TableStyle-DetailedwithPadding" cellspacing="0">
            <col style="width: 50pt;" class="TableStyle-DetailedwithPadding-Column-Column1" />
            <col class="TableStyle-DetailedwithPadding-Column-Column1" />
            <col class="TableStyle-DetailedwithPadding-Column-Column1" />
            <col class="TableStyle-DetailedwithPadding-Column-Column1" />
            <thead>
                <tr class="TableStyle-DetailedwithPadding-Head-Header1">
                    <th class="TableStyle-DetailedwithPadding-HeadD-Column1-Header1" colspan="4">Provded Service (storage)</th>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Head-Header1">
                    <td class="TableStyle-DetailedwithPadding-HeadE-Column1-Header1">Flavor</td>
                    <td class="TableStyle-DetailedwithPadding-HeadE-Column1-Header1">Has Raft Map</td>
                    <td class="TableStyle-DetailedwithPadding-HeadE-Column1-Header1">Number of Raft sessions</td>
                    <td class="TableStyle-DetailedwithPadding-HeadD-Column1-Header1">Container Names</td>
                </tr>
            </thead>
            <tbody>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">Bucket</td>
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">Yes</td>
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">1(map) + 8</td>
                    <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">scality-metadata-bucket-map
 + scality-metadata-bucket-repd</td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td class="TableStyle-DetailedwithPadding-BodyB-Column1-Body1">Vault</td>
                    <td class="TableStyle-DetailedwithPadding-BodyB-Column1-Body1">Yes</td>
                    <td class="TableStyle-DetailedwithPadding-BodyB-Column1-Body1">1(map) + 0</td>
                    <td class="TableStyle-DetailedwithPadding-BodyA-Column1-Body1">scality-metadata-vault-map</td>
                </tr>
            </tbody>
        </table>
        <table style="width: 100%;mc-table-style: url('../Resources/TableStyles/DetailedwithPadding.css');margin-left: auto;margin-right: auto;" class="TableStyle-DetailedwithPadding" cellspacing="0">
            <col style="width: 50pt;" class="TableStyle-DetailedwithPadding-Column-Column1" />
            <col class="TableStyle-DetailedwithPadding-Column-Column1" style="width: 120px;" />
            <col class="TableStyle-DetailedwithPadding-Column-Column1" />
            <thead>
                <tr class="TableStyle-DetailedwithPadding-Head-Header1">
                    <th class="TableStyle-DetailedwithPadding-HeadD-Column1-Header1" colspan="3">Storage Configuration and Features</th>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Head-Header1">
                    <td class="TableStyle-DetailedwithPadding-HeadE-Column1-Header1">
                        Flavor
                    </td>
                    <td class="TableStyle-DetailedwithPadding-HeadE-Column1-Header1">
                        Multi-disk support
                    </td>
                    <td class="TableStyle-DetailedwithPadding-HeadD-Column1-Header1">
                        Configuration setting name
                    </td>
                </tr>
            </thead>
            <tbody>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        Bucket
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        Yes
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
                        env_metadata_bucket_datapaths
                    </td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td class="TableStyle-DetailedwithPadding-BodyB-Column1-Body1">
                        Vault
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyB-Column1-Body1">
                        No
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyA-Column1-Body1">
                       env_host_data
                    </td>
                </tr>
            </tbody>
        </table>
        <table style="width: 100%;mc-table-style: url('../Resources/TableStyles/DetailedwithPadding.css');margin-left: auto;margin-right: auto;" class="TableStyle-DetailedwithPadding" cellspacing="0">
            <col style="width: 50pt;" class="TableStyle-DetailedwithPadding-Column-Column1" />
            <col class="TableStyle-DetailedwithPadding-Column-Column1" />
            <col class="TableStyle-DetailedwithPadding-Column-Column1" />
            <thead>
                <tr class="TableStyle-DetailedwithPadding-Head-Header1">
                    <th class="TableStyle-DetailedwithPadding-HeadD-Column1-Header1" colspan="3">Data Storage Paths</th>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Head-Header1">
                    <th class="TableStyle-DetailedwithPadding-HeadE-Column1-Header1">
                        Flavor
                    </th>
                    <th class="TableStyle-DetailedwithPadding-HeadE-Column1-Header1">
                        Component-specific path name (env_host_*)                    </th>
                    <th class="TableStyle-DetailedwithPadding-HeadD-Column1-Header1">
                        Component-specific basename for storage                    </th>
                </tr>
            </thead>
            <tbody>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        Bucket
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        scality-metadata-bucket
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
                        scality-metadata-databases-bucket
                    </td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td class="TableStyle-DetailedwithPadding-BodyB-Column1-Body1">
                        Vault
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyB-Column1-Body1">
                        scality-metadata-vault
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyA-Column1-Body1">
                        scality-metadata-databases-vault
                    </td>
                </tr>
            </tbody>
        </table>
        <h3 MadCap:autonum="1.5.2 &#160;">Metadata Storage File Layout</h3>
        <p style="page-break-after: avoid;">With the differences clarified between <i>bucket storage</i> (the bucket flavor of the Metadata component) and  <i>vault storage</i>, it is possible to examine the specifics of Metadata storage file layout in the <span class="ProprietaryFileName">group_vars/all</span> file.</p><pre xml:space="preserve">env_host_data: /scality/s3/data
env_metadata_bucket_datapaths:
  - /scality/s3/disk1
  - /scality/s3/disk2</pre>
        <p class="BoxNote">Vault-flavor Metadata files have a similar layout to bucket-flavor files, except that in a configuration, the data storage mount point is unique (a list containing one item) rather than multiple (a list containing <i>N</i> items).</p>
        <p style="page-break-after: avoid;">On each mount point configured as storage for the Metadata component, there is a directory that carries the flavor name <b>component-specific basename for storage</b>.</p>
        <table style="width: 100%;margin-left: auto;margin-right: auto;mc-table-style: url('../Resources/TableStyles/DetailedwithPadding.css');" class="TableStyle-DetailedwithPadding" cellspacing="0">
            <col style="width: 50pt;" class="TableStyle-DetailedwithPadding-Column-Column1" />
            <col class="TableStyle-DetailedwithPadding-Column-Column1" />
            <thead>
                <tr class="TableStyle-DetailedwithPadding-Head-Header1">
                    <td class="TableStyle-DetailedwithPadding-HeadE-Column1-Header1">
                        Flavor
                    </td>
                    <td class="TableStyle-DetailedwithPadding-HeadD-Column1-Header1">
                        Component-specific basename for storage
                    </td>
                </tr>
            </thead>
            <tbody>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        Bucket
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
                        scality-metadata-databases-bucket
                    </td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td class="TableStyle-DetailedwithPadding-BodyB-Column1-Body1">
                        Vault
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyA-Column1-Body1">
                        scality-metadata-databases-vault
                    </td>
                </tr>
            </tbody>
        </table>
        <p>Thus, the following paths exist on the host:</p><pre>/scality/s3/data/scality-metadata-databases-vault/</pre><pre>/scality/s3/disk1/scality-metadata-databases-bucket/</pre><pre xml:space="preserve">/scality/s3/disk2/scality-metadata-databases-bucket/</pre>
        <h3 MadCap:autonum="1.5.3 &#160;">Metadata Bind-Mount Technique for Consistent DB View</h3>
        <p>Bind mounts are used to link paths together to aggregate the various volumes  in the service into one consistent view between the Host and the container. These bind mounts are located under the <b>component-specific basename</b> in the mount point specified by the <b>env_host_data</b> ansible variable. The advantage of this method over directly aggregating the mount points to the Docker container is that it allows the same view from both the container <span class="ProprietaryFileName">/databases</span> path and from the hosts <b>component-specific data</b> path (<span class="ProprietaryFileName">/scality/s3/data/scality-metadata-bucket/databases</span>, as shown below). This makes standard LevelDB tools usable from the host file system even when the containers are not running, and eases investigative operations with no impact on the service’s functional aspects.</p>
        <p>Because only the Metadata “Bucket” flavor makes full use of the bind-mount technique, it provides the best example. Consider the common database path on the host for metadata-bucket containers:</p><pre>/scality/s3/data/scality-metadata-bucket/databases/</pre>
        <p>This directory contains subdirectories bound to their associated storage directories and disks. </p><pre>$&gt; ls /scality/s3/data/scality-metadata-bucket/databases/
disk1
disk2</pre>
        <p style="page-break-after: avoid;">Reviewing <span class="ProprietaryFileName">/etc/fstab</span> confirms the bind-mounts’ existence:</p><pre style="page-break-inside: avoid;">$&gt; cat /etc/fstab
/scality/s3/disk1/scality-metadata-databases-bucket /scality/s3/data/scality-metadata-bucket/databases/disk1 none bind 0 0
/scality/s3/disk2/scality-metadata-databases-bucket /scality/s3/data/scality-metadata-bucket/databases/disk2 none bind 0 0</pre>
        <p>Because <span class="ProprietaryFileName">/scality/s3/data/scality-metadata-bucket/databases/</span> is mounted as the <span class="ProprietaryFileName">/database </span>directory into the <span class="ProprietaryNonConventional">scality-metadata-bucket-*</span> containers, the same view of the data is available from the host in <span class="ProprietaryFileName">/scality/s3/data/scality-metadata-bucket/databases/</span> and from the container in <span class="ProprietaryFileName">/databases</span>.</p>
        <h3 MadCap:autonum="1.5.4 &#160;">Metadata Multi-Disk Support Internals</h3>
        <p>The consistent view enabled by the mount-point layout is essential to multi-disk support. This consistency enables LevelDB tools to get information either from within the container or the host.</p>
        <h4>Disk Usage</h4>
        <p>S3 Connector multi-disk support spreads internal LevelDB files over the disks provided to the component. It does this in a way that allows the use of standard LevelDB tools, assuming:</p>
        <ul>
            <li>Any file spread to another disk supplies a relative symlink pointing to it from the original <span class="ProprietaryFileName">leveldb</span> directory (along with the consistent view of the mount point layout, this allows equivalent resolution of links in both the host and the container)</li>
            <li>The main metadata has been moved from the top-level directory to a <span class="ProprietaryFileName"> main</span> directory</li>
            <li>The <span class="ProprietaryFileName">main</span> directory is linked to by  like-named relative symlinks on every disk (allowing quick access to the data without manual lookups).</li>
            <li>The actual data (.ldb) files were moved from the top-level directory to the <span class="ProprietaryFileName">data</span> directories in every disk.</li>
        </ul>
        <p style="page-break-after: avoid;">The resulting file structure resembles the one shown here.</p><pre>$ tree /scality/s3/data/scality-metadata-bucket/databases/
			
/scality/s3/data/scality-metadata-bucket/databases/
└── disk1
    └── 0
         └── 25
             └── storeDb
		├── 000001.ldb -&gt; ../../../../../disk2/0/25/storeDb0/data/000001.ldb
		├── 000003.ldb -&gt; data/000003.ldb
		├── 000005.log -&gt; main/000005.log
		├── CURRENT -&gt; main/CURRENT
		├── data
		│   └── 000003.ldb
		├── LOCK
		├── LOG
		├── LOG.old
		├── main
		│   ├── 000005.log
		│   ├── CURRENT
		│   └── MANIFEST-000004
		└── MANIFEST-000004 -&gt; main/MANIFEST-000004
└── disk2
	└── 0
	    └── 25
		└── storeDb0
		    ├── main -&gt; ../../../../../disk1/0/25/storeDb0/main
		    └── data
                         └── 000001.ldb</pre>
        <p style="page-break-after: avoid;">These paths contain directories named <span class="ProprietaryFileName">0</span>, <span class="ProprietaryFileName">25</span>, and <span class="ProprietaryFileName">storeDb0.</span></p>
        <table style="width: 100%;margin-left: auto;margin-right: auto;mc-table-style: url('../Resources/TableStyles/SimpleDefinitions.css');" class="TableStyle-SimpleDefinitions" cellspacing="0">
            <col class="TableStyle-SimpleDefinitions-Column-Column1" style="width: 70pt;" />
            <col class="TableStyle-SimpleDefinitions-Column-Column1" style="width: 494px;" />
            <tbody>
                <tr class="TableStyle-SimpleDefinitions-Body-Row1">
                    <td class="TableStyle-SimpleDefinitions-BodyE-Column1-Row1">
                        0
                    </td>
                    <td class="TableStyle-SimpleDefinitions-BodyD-Column1-Row1">
                        Raft session ID: 0 indicates the map, and extends sequentially up to 1 + (8 * number of clusters)
                    </td>
                </tr>
                <tr class="TableStyle-SimpleDefinitions-Body-Row1">
                    <td class="TableStyle-SimpleDefinitions-BodyE-Column1-Row1">
                        25
                    </td>
                    <td class="TableStyle-SimpleDefinitions-BodyD-Column1-Row1">
                        InstallID:  Usually 0 in customer deployments (set to 25 for the present example to distinguish it from the Raft session ID)
                    </td>
                </tr>
                <tr class="TableStyle-SimpleDefinitions-Body-Row1">
                    <td class="TableStyle-SimpleDefinitions-BodyB-Column1-Row1">
                        storeDb0
                    </td>
                    <td class="TableStyle-SimpleDefinitions-BodyA-Column1-Row1">
                        Name of the actual LevelDB database. For buckets pre-dating version 7.2, this name matches the S3 bucket name. Any bucket created after version 7.2 is included in one of the 64 available storeDbs per Raft session.
                    </td>
                </tr>
            </tbody>
        </table>
        <p>The system enables the use of standard LevelDB tools from the host, but only when the containers are stopped (due to the process-exclusive aspect of accessing a LevelDB database).</p>
        <h4>File Spreading Methods</h4>
        <p>The multi-disk support feature enables two types of spreading: spreading top-level internal LevelDB data over multiple disks, and spreading internal LevelDB data (.ldb) files.</p>
        <h5>Bucket/Database Association</h5>
        <p>With multi-disk support (version 7.2 and later), the Metadata service can handle unlimited buckets. Rather than keep one LevelDB database per bucket, Metadata aggregates buckets into large LevelDB databases that spread their data over the available mount points. For each Raft session (thus per repd process), 64 aggregator DBs, named <b>storeDbX</b> (with X—the DB index—ranging from 0 to 63, inclusive), are available. Thus, the Metadata service logic can access one bucket without considering the distribution of the buckets over the DBs.</p>
        <h5>Spreading the LevelDB Metadata Files</h5>
        <p>Whenever Metadata creates a database, it randomly selects a mount point to hold the database’s metadata files. These are special within LevelDB, due to their uniqueness and core role in the internals of LevelDB. Among those files, the following are particularly important:</p>
        <table style="mc-table-style: url('../Resources/TableStyles/SimpleDefinitions.css');margin-left: auto;margin-right: auto;width: 100%;" class="TableStyle-SimpleDefinitions" cellspacing="0">
            <col class="TableStyle-SimpleDefinitions-Column-Column1" style="width: 120px;" />
            <col class="TableStyle-SimpleDefinitions-Column-Column1" style="width: 528px;" />
            <tr class="TableStyle-SimpleDefinitions-Body-Row1">
                <td class="TableStyle-SimpleDefinitions-BodyE-Column1-Row1">MANIFEST-XXX</td>
                <td class="TableStyle-SimpleDefinitions-BodyD-Column1-Row1">Index of the internal files constituting the database.</td>
            </tr>
            <tr class="TableStyle-SimpleDefinitions-Body-Row1">
                <td class="TableStyle-SimpleDefinitions-BodyE-Column1-Row1">CURRENT</td>
                <td class="TableStyle-SimpleDefinitions-BodyD-Column1-Row1">Points to the currently valid MANIFEST-XXX file.</td>
            </tr>
            <tr class="TableStyle-SimpleDefinitions-Body-Row1">
                <td class="TableStyle-SimpleDefinitions-BodyE-Column1-Row1">XXXXXX.log</td>
                <td class="TableStyle-SimpleDefinitions-BodyD-Column1-Row1">Contains all very recently-written data (puts/dels), until compacting of the file into .ldb files. All writes go through this file.</td>
            </tr>
        </table>
        <p>This randomness ensures that writes to all DBs do not go through a single mount point.</p>
        <h5>Spreading the .LDB Files</h5>
        <p style="page-break-after: avoid;">The .ldb file-spreading method must work within the following constraints:</p>
        <ul>
            <li>Avoid overloading one disk when it is almost empty and others are almost full (extension of the storage)</li>
            <li>Cannot rely on logic that is altered depending on the number of disks</li>
            <li>Ensure compatibility with existing LevelDB tools</li>
        </ul>
        <p>To work within these constraints, the design sets symlinks from the original database’s main directory to the .ldb files, ensuring that standard tools can find data and work seamlessly.</p>
        <p>Randomness-based selection and aggregating buckets into a few levelDBs ensure that there will be enough .ldb files per database to guarantee that files will be spread evenly (statistically speaking) throughout the available mount points.</p>
        <h3 MadCap:autonum="1.5.5 &#160;">Metadata Pruning</h3>
        <p>Pruning is the removal of obsolete and no-longer-useful data from a data set. In the context of a Metadata cluster, pruning refers to the Raft log entries already backed up to a RING. Because relevant log entries are available from blobs already stored in the RING, the Metadata service no longer requires them in the local Raft log, and can thus free disk space on its SSDs, holding the log to a reasonable size.</p>
        <p>Once a metadata server has successfully backed its local journal up, the local copy is no longer necessary and can be pruned to reclaim space.</p>
        <h4>Configuring Metadata Pruning</h4>
        <p>The Metadata pruning feature is not configurable. It is enabled through the Federation environment’s <span class="ProprietaryFileName">group_vars/all</span> file, through the <code>prune</code> parameter of the <span class="ElementName">env_metadata</span> section:</p><pre xml:space="preserve">env_metadata:
   # enable / disable autostart for filebeat
   filebeat_autostart: true
   # enable / disable autorestart for filebeat
   filebeat_autorestart: true
   # enable / disable prunning
   prune: false</pre>
        <p> The default value for the pruning feature is disabled (false). To enable pruning, set this value to true. </p>
        <h4>Enabling Metadata Pruning from ansible-playbook</h4>
        <p>The following commands enable or disable Metadata pruning using ansible-playbook:</p>
        <p>Enable pruning:</p><pre>
./ansible-playbook -i env/&lt;my-env&gt;/inventory tooling-playbooks/metadata-activate-pruning.yml
</pre>
        <p>Disable pruning:</p><pre>
./ansible-playbook -i env/&lt;my-env&gt;/inventory tooling-playbooks/metadata-deactivate-pruning.yml</pre>
        <p>Invoking the <span class="Code_Terminal">-t</span> option with the following tags limits the scope for both Ansible playbooks:</p>
        <ul>
            <li><code>bucket</code> for bucket-related metadata</li>
            <li><code>vault</code> for vault-related metadata</li>
        </ul>
        <p>This setting persists across server, Docker, or process restarts; however, the Federation <span class="ProprietaryFileName">run.yml</span> playbook overrides it.</p>
    </body>
</html>