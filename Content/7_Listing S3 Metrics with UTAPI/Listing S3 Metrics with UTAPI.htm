﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd">
    <head>
        <link href="../Resources/TableStyles/DetailedwithPadding.css" rel="stylesheet" MadCap:stylesheetType="table" />
    </head>
    <body>
        <h1 MadCap:autonum="1. &#160;">Listing S3 Metrics with UTAPI</h1>
        <p style="text-align: left;">The Service Utilization API (UTAPI) enables resource utilization tracking and metrics reporting. UTAPI can accept a request signed using v4 authentication for the REST API calls listed in the <span class="PublicationName"><MadCap:variable name="General.ReferenceManual" /></span>.</p>
        <p style="text-align: left;">To make a successful request to UTAPI:</p>
        <ul>
            <li style="text-align: left;">Create an IAM user with a policy giving access to UTAPI</li>
            <li style="text-align: left;">Sign a request with V4 authentication</li>
        </ul>
        <h2 style="text-align: left;" MadCap:autonum="1.1. &#160;">Creating an IAM User with Policy Access to UTAPI</h2>
        <p class="BoxNote">The examples here use AWS CLI but any AWS SDK will work with these actions.</p>
        <ol>
            <li>Create an IAM user:<pre xml:space="preserve">$ aws iam --endpoint-url &lt;endpoint&gt; create-user --user-name utapiuser</pre></li>
            <li>Create an access key for the user:<pre xml:space="preserve">$ aws iam --endpoint-url &lt;endpoint&gt; create-access-key --user-name utapiuser</pre></li>
            <li>
                <p style="page-break-after: avoid;">Create a managed IAM policy:</p>
                <p class="Strong" style="font-style: italic;page-break-after: avoid;">Sample UTAPI policy</p><pre xml:space="preserve">$ cat - &gt; utapipolicy.json &lt;&lt;;EOF
{
&#160;&#160;"Version": "2012-10-17",
&#160;&#160;"Statement": [
&#160;&#160;&#160;&#160;{
&#160;&#160;&#160;&#160;"Sid": "utapiMetrics",
&#160;&#160;&#160;&#160;"Action": [ "utapi:ListMetrics" ],
&#160;&#160;&#160;&#160;"Effect": "Allow",
&#160;&#160;&#160;&#160;"Resource": "arn:scality:utapi:::buckets/*"
&#160;&#160;&#160;&#160;}
&#160;&#160;]
}
EOF</pre>
                <p>The user can also be restricted to only some buckets (the foo bucket for example) by changing the <span class="Code_Terminal">Resource</span> property to <span class="Code_Terminal">"Resource": "arn:scality:utapi:::buckets/foo"</span></p><pre xml:space="preserve">$ aws iam --endpoint-url &lt;endpoint&gt; create-policy --policy-name utapipolicy --policy-document file://utapipolicy.json</pre>
                <p style="page-break-after: avoid;">The output of the above command looks like:</p><pre>{
&#160;&#160;"Policy": {
&#160;&#160;&#160;&#160;"PolicyName": "utapipolicy",
&#160;&#160;&#160;&#160;"CreateDate": "2017-06-01T19:31:18.620Z",
&#160;&#160;&#160;&#160;"AttachmentCount": 0,
&#160;&#160;&#160;&#160;"IsAttachable": true,
&#160;&#160;&#160;&#160;"PolicyId": "ZXR6A36LTYANPAI7NJ5UV",
&#160;&#160;&#160;&#160;"DefaultVersionId": "v1",
&#160;&#160;&#160;&#160;"Path": "/",
&#160;&#160;&#160;&#160;"Arn": "arn:aws:iam::0123456789012:policy/utapipolicy",
&#160;&#160;&#160;&#160;"UpdateDate": "2017-06-01T19:31:18.620Z"
&#160;&#160;}
}</pre>
                <p>The <span class="Code_Terminal">arn</span> property of the response is used in the next step to attach the policy to the user.</p>
            </li>
            <li>
                <p style="page-break-after: avoid;">Attach the user to the managed policy:</p><pre xml:space="preserve">$ aws --endpoint-url &lt;endpoint&gt; iam  attach-user-policy --user-name utapiuser --policy-arn &lt;policy arn&gt;</pre>
                <p>Now the user utapiuser has access to ListMetrics request in UTAPI&#160;on all buckets.</p>
            </li>
        </ol>
        <h2 MadCap:autonum="1.2. &#160;">Metrics Start and End Times</h2>
        <p style="text-align: left;">Both start and end times are expressed as UNIX epoch time stamps in milliseconds.</p>
        <p style="text-align: left;">Because UTAPI metrics are normalized to the nearest 15-minute interval, start time and end time must be in the following format:</p>
        <ul>
            <li><b>Start time</b> Start time must be normalized to the nearest 15-minute interval with seconds and milliseconds set to 0. Valid start time stamps, therefore, look like <span class="Code_Terminal">09:00:00:000, 09:15:00:000, 09:30:00:000</span> and <span class="Code_Terminal">09:45:00:000</span>.
        <p>For example:</p><pre>Date: Tue Oct 11 2016 17:35:25 GMT-0700 (PDT)
Unix timestamp (milliseconds): 1476232525320</pre><p>This is a typical JavaScript method to get the start time stamp and change it to the epoch time format (<span class="Code_Terminal">1476231300000</span> in the above example):</p><pre>function getStartTimestamp(t) {
&#160;&#160;const time = new Date(t);
&#160;&#160;const minutes = time.getMinutes();
&#160;&#160;const timestamp = time.setMinutes((minutes - minutes % 15), 0, 0);
&#160;&#160;return timestamp;
},</pre></li>
            <li>
                <p><b>End time</b> End time must be normalized to the nearest 15-minute end interval with seconds and milliseconds set to 59 and 999, respectively. Valid end time stamps, therefore, look like <span class="Code_Terminal">09:14:59:999, 09:29:59:999, 09:44:59:999</span> and <span class="Code_Terminal">09:59:59:999</span>.</p>
                <p>This is a typical JavaScript method to get the end time stamp and change it to the epoch time format (<span class="Code_Terminal">1476233099999</span> in the above example):</p><pre>function getEndTimestamp(t) {
&#160;&#160;const time = new Date(t);
&#160;&#160;const minutes = time.getMinutes();
&#160;&#160;const timestamp = time.setMinutes((minutes - minutes % 15) + 15, 0, -1);
&#160;&#160;return timestamp;
}</pre>
                <p>Use the command-line tool available in Scality S3 to list UTAPI metrics with start and end times.</p>
            </li>
        </ul>
        <p>If  S3 is running inside a Docker container, <span class="Code_Terminal">docker exec</span> into the S3 container as:</p><pre xml:space="preserve">$ docker exec -it &lt;container id&gt; bash</pre>
        <p>and then run the command:</p><pre xml:space="preserve">$ node bin/list_metrics</pre>
        <p style="page-break-after: avoid;">This generates the following output, listing available options:</p><pre>Usage: list_metrics [options]</pre>
        <table style="width: 100%;mc-table-style: url('../Resources/TableStyles/DetailedwithPadding.css');" class="TableStyle-DetailedwithPadding" cellspacing="0">
            <col style="width: 238px;" class="TableStyle-DetailedwithPadding-Column-Column1" />
            <col class="TableStyle-DetailedwithPadding-Column-Column1" style="width: 404px;" />
            <thead>
                <tr class="TableStyle-DetailedwithPadding-Head-Header1">
                    <th class="TableStyle-DetailedwithPadding-HeadE-Column1-Header1">Option</th>
                    <th class="TableStyle-DetailedwithPadding-HeadD-Column1-Header1">Description</th>
                </tr>
            </thead>
            <tbody>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td style="text-align: left;" class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1"><code>-h, --help </code>
                    </td>
                    <td style="text-align: left;" class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">Output usage information</td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td style="text-align: left;" class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1"><code>-V, --version</code>
                    </td>
                    <td style="text-align: left;" class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">Output the version number</td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td style="text-align: left;" class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1"><code>-a, --access-key &lt;accessKey&gt;</code>
                    </td>
                    <td style="text-align: left;" class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">Access key id</td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td style="text-align: left;" class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1"><code>-k, --secret-key &lt;secretKey&gt;</code>
                    </td>
                    <td style="text-align: left;" class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">Secret access key</td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td style="text-align: left;" class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1"><code>--buckets &lt;buckets&gt;</code>
                    </td>
                    <td style="text-align: left;" class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">Name of bucket(s), with a comma separator if more than one</td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td style="text-align: left;" class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1"><code>--accounts &lt;accounts&gt;</code>
                    </td>
                    <td style="text-align: left;" class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">Account ID(s) with a comma separator if more than one</td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td style="text-align: left;" class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1"><code>--users &lt;users&gt;</code>
                    </td>
                    <td style="text-align: left;" class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">User ID(s) with a comma separator if more than one</td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td style="text-align: left;" class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1"><code>--service &lt;service&gt;</code>
                    </td>
                    <td style="text-align: left;" class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">Name of service</td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td style="text-align: left;" class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1"><code>-s, --start &lt;start&gt;</code>
                    </td>
                    <td style="text-align: left;" class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">Start of time range</td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td style="text-align: left;" class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1"><code>-r, --recent</code>
                    </td>
                    <td style="text-align: left;" class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">List metrics including the previous and current 15 minute interval</td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td style="text-align: left;" class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1"><code>-e, --end &lt;end&gt;</code>
                    </td>
                    <td style="text-align: left;" class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">End of time range</td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td style="text-align: left;" class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1"><code>-h, --host &lt;host&gt;</code>
                    </td>
                    <td style="text-align: left;" class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">Host of the server</td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td style="text-align: left;" class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1"><code>-p, --port &lt;port&gt;</code>
                    </td>
                    <td style="text-align: left;" class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">Port of the server</td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td style="text-align: left;" class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1"><code>--ssl</code>
                    </td>
                    <td style="text-align: left;" class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">Enable ssl</td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td style="text-align: left;" class="TableStyle-DetailedwithPadding-BodyB-Column1-Body1"><code>-v, --verbose</code>
                    </td>
                    <td style="text-align: left;" class="TableStyle-DetailedwithPadding-BodyA-Column1-Body1">Show more detail</td>
                </tr>
            </tbody>
        </table>
        <p style="text-align: left;page-break-after: avoid;">A typical call (to list metrics for a bucket demo to UTAPI in an HTTPS-enabled deployment) is:</p><pre xml:space="preserve">$ node bin/list_metrics --metric buckets --buckets demo --start 1476231300000 <br />--end 1476233099999 -a myAccessKey -k mySecretKey -h 127.0.0.1 -p 8100 --ssl</pre>
    </body>
</html>