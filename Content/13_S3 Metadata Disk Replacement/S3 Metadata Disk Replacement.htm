﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd">
    <head>
        <link href="../Resources/TableStyles/SimpleDefinitions.css" rel="stylesheet" MadCap:stylesheetType="table" />
    </head>
    <body>
        <h1 MadCap:autonum="1. &#160;"><a name="Metadata"></a>Metadata Disk Replacement</h1>
        <p class="BoxIMPORTANT">Scality strongly recommends installing the Metadata service and operating system on different disks.</p>
        <p class="Alert" style="text-align: left;">If it is necessary to change the server or reinstall the operating system, be sure to retain the IP address. </p>
        <p style="text-align: left;">For instructions on installing the S3 Metadata service, see the <span class="PublicationName"><MadCap:variable name="General.SetupInstall"></MadCap:variable>.</span></p>
        <h2 MadCap:autonum="1.1. &#160;"><a name="Retrievi"></a>Retrieving Mount Points and Disks Associated with  S3 Service Components</h2>
        <p>See the <span class="ProprietaryFileName">env/{{ENV_NAME}}/group_vars/all</span> file in the Federation installation folder to locate the original mount points.</p>
        <p>If using an offline installer, run the following command to put Ansible in the path:</p><pre xml:space="preserve">$ source $S3_OFFLINE_DIR/repo/venv/bin/activate</pre>
        <p>Release 7.2 introduced an Ansible setting to configure data storage locations for bucket storage (<b>metadata-bucket</b> containers). </p>
        <p>Four commands retrieve the mount points. They differ only in the names of the four settings invoked:</p>
        <table style="mc-table-style: url('../Resources/TableStyles/SimpleDefinitions.css');margin-left: auto;margin-right: auto;width: 100%;" class="TableStyle-SimpleDefinitions" cellspacing="0">
            <col class="TableStyle-SimpleDefinitions-Column-Column1" />
            <col class="TableStyle-SimpleDefinitions-Column-Column1" />
            <tr class="TableStyle-SimpleDefinitions-Body-Row1">
                <td class="TableStyle-SimpleDefinitions-BodyE-Column1-Row1">
                    <p style="text-align: center;"><b>env_host_conf</b> (single path)</p>
                </td>
                <td class="TableStyle-SimpleDefinitions-BodyD-Column1-Row1">Specifies the storage mount point of all S3 service configuration files.</td>
            </tr>
            <tr class="TableStyle-SimpleDefinitions-Body-Row1">
                <td class="TableStyle-SimpleDefinitions-BodyE-Column1-Row1">
                    <p style="text-align: center;"><b>env_host_logs</b> (single path)</p>
                </td>
                <td class="TableStyle-SimpleDefinitions-BodyD-Column1-Row1">Specifies the storage mount point of all S3 service logs.</td>
            </tr>
            <tr class="TableStyle-SimpleDefinitions-Body-Row1">
                <td class="TableStyle-SimpleDefinitions-BodyE-Column1-Row1">
                    <p style="text-align: center;"><b>env_host_data</b> (single path)</p>
                </td>
                <td class="TableStyle-SimpleDefinitions-BodyD-Column1-Row1">Specifies the storage mount point of most S3 service config files and data.</td>
            </tr>
            <tr class="TableStyle-SimpleDefinitions-Body-Row1">
                <td class="TableStyle-SimpleDefinitions-BodyE-Column1-Row1">
                    <p style="text-align: center;"><b>env_metadata_bucket_datapaths</b> (YAML list of paths)</p>
                </td>
                <td class="TableStyle-SimpleDefinitions-BodyD-Column1-Row1">Specifies the storage mount point(s) for bucket storage.</td>
            </tr>
        </table>
        <p>All the following commands have Ansible-styled output, and output values as JSON. This is mostly relevant for the list of paths contained in <span class="HTMLCode">env_metadata_bucket_datapaths</span>.</p>
        <p style="page-break-after: avoid;">Command to retrieve <span class="HTMLCode">env_host_data</span>:</p><pre xml:space="preserve">$ ansible -i env/{{ENV_NAME}}/inventory.1site '127.0.0.1' -c local -m debug -a var=env_host_data</pre>
        <p>Command to retrieve <span class="HTMLCode">env_host_conf</span>:</p><pre xml:space="preserve">$ ansible -i env/{{ENV_NAME}}/inventory.1site '127.0.0.1' -c local -m debug -a var=env_host_conf</pre>
        <p>Command to retrieve <span class="HTMLCode">env_host_logs</span>:</p><pre xml:space="preserve">$ ansible -i env/{{ENV_NAME}}/inventory.1site '127.0.0.1' -c local -m debug -a var=env_host_logs</pre>
        <p>Command to retrieve <span class="HTMLCode">env_metadata_bucket_datapaths</span>:</p><pre xml:space="preserve">$ ansible -i env/{{ENV_NAME}}/inventory.1site '127.0.0.1' -c local -m debug -a var=env_metadata_bucket_datapaths</pre>
        <h2 MadCap:autonum="1.2. &#160;">Actions to Take Before Installing S3 Service Stateful Components</h2>
        <p>For SSDs in <span class="HTMLCode">env_host_logs</span>, <span class="HTMLCode">env_host_conf</span>, <span class="HTMLCode">env_host_data</span>, or <span class="HTMLCode">env_metadata_bucket_datapaths</span>:</p>
        <ul>
            <li>For SSDs shared with the RING, ensure that when the machine reboots, Docker starts only after the RING StoreNode has started, so that StoreNode can mount the disks first.</li>
            <li>For SSDs dedicated to S3 only, mount the associated disk in the server at the configured mount point, and ensure  that when the machine reboots that the mount point is mounted before Docker starts, when the server restarts.</li>
        </ul>
        <p>A dedicated disk for logs can also be put in place. </p>
        <h2 MadCap:autonum="1.3. &#160;">Replacing a Lost S3 Service Stateful Component Data Disk</h2>
        <p class="Note">See <MadCap:xref href="#Retrievi" class="section_number">section 1.1</MadCap:xref> for information on how to retrieve the current mount points.</p>
        <p>If the lost SSD is included in the list of mount points provided for <span class="HTMLCode">env_metadata_bucket_datapaths</span> and it is dedicated solely to metadata, mount the new disk to the original mount point and follow the instructions for replacing a lost Metatdata disk in <MadCap:xref href="#Replacin" class="section_number">section 1.3.1</MadCap:xref> (if the SSD is not dedicated to Metadata only, refer to the RING procedure for replacing a disk).</p>
        <p>If the lost SSD is defined as the <span class="HTMLCode">env_host_data</span>, <span class="HTMLCode">env_host_conf</span>, or <span class="HTMLCode">env_host_log</span> mount point, follow the instructions for replacing a lost S3 service disk (<MadCap:xref href="#Replacin2" class="section_number">section 1.3.2</MadCap:xref>)</p>
        <p>Once the SSD replacement is complete, rerun the Metadata service (see section <MadCap:xref href="#Rerun" class="section_number">section 1.4</MadCap:xref>) to finish the replacement procedure.</p>
        <h3 MadCap:autonum="1.3.1 &#160;"><a name="Replacin"></a>Replacing a Lost Metadata Disk</h3>
        <p>An option is available that allows the Metadata service to store data on multiple mount points. Because this mechanism is similar to a RAID0, losing a mount point (or disk) means losing access to the whole Metadata (bucket storage) data set. As a result, if any of the mount points of <span class="ElementName">env_metadata_bucket_datapaths</span> is lost, the entire set must be wiped before proceeding further.</p>
        <p>On the machine that lost one or more disks for each mount point contained in <span class="ElementName">env_metadata_bucket_datapaths</span> that is still mounted and available, run the following command for each directory in <span class="ElementName">env_metadata_bucket_datapaths</span> on the machine that has the SSD replaced:</p><pre xml:space="preserve">$ rm -rf &lt;&lt;directory&gt;&gt;/scality-metadata-databases-bucket/*</pre>
        <p>The Ansible binary must be in the path for the command to work.</p>
        <p>This command wipes all data and state from the bucket storage container, leaving it ready to re-synchronize with the cluster. </p>
        <p>Rerun the Metadata service (as described in <MadCap:xref href="#Rerun">“Rerun the Metadata Service” on page&#160;1</MadCap:xref>), following the steps for re-creating directories and restarting the service on the impaired machine.</p>
        <h3 MadCap:autonum="1.3.2 &#160;"><a name="Replacin2"></a>Replacing a Lost S3 Service Disk</h3>
        <p>A disk loss may affect three mount points at the S3 service level: <span class="ElementName">env_host_logs</span>, <span class="ElementName">env_host_conf</span>, and <span class="ElementName">env_host_data</span>. These mount points should be treated in the same manner.</p>
        <ol>
            <li>Stop Docker.</li>
            <li>Replace the disk.</li>
            <li>Re-create the file-system for the logs.</li>
            <li>Mount the disk to the associated mount point (if the disk is not shared with the RING).</li>
            <li>Before Docker restarts, confirm that the mount point is available.</li>
            <li>If one of the <span class="ElementName">env_metadata_bucket_datapaths</span> mount points is also affected, make sure to follow the steps from the relevant section.</li>
        </ol>
        <p>Finally, rerun the Metadata service (see <MadCap:xref href="#Retrievi" class="section_number">section 1.1</MadCap:xref>), following the steps for re-creating directories with proper permissions, according to the environment configuration.</p>
        <h2 MadCap:autonum="1.4. &#160;"><a name="Rerun"></a>Rerun the Metadata Service</h2>
        <p>To reinstall the software on the lost machine, run the following command from the Federation directory:</p><pre xml:space="preserve">$ ansible-playbook -i $MY_INVENTORY_FILE run.yml -l {{SERVER_HOSTNAME}}</pre>
        <p style="text-align: left;widows: 2;orphans: 5;">When Federation starts the Metadata containers, the node loads its data automatically from the backup. To ensure this, tail the logs located in the path: <span class="ProprietaryFileName">{{env_host_logs}}/scality-metadata/logs</span>. To find <span class="Variable">{{env_host_logs}}</span>, follow the instructions above.</p>
        <p style="text-align: left; widows: 2; orphans: 5;">To check that the Metadata node caught everything, run the following command:</p><pre xml:space="preserve">$ ansible-playbook -i $MY_INVENTORY_FILE tooling-playbooks/check-status-metadata.yml</pre>
        <p>This command dumps to <span class="ProprietaryFileName">/tmp/results.txt</span> and lists every Raft session state. </p>
        <p>Search <span class="ProprietaryFileName">results.txt</span> for aseq, bseq, and cseq.</p>
        <ul>
            <li><span class="Code_Terminal">bseq</span> indicates the number of backup entries loaded</li>
            <li><span class="Code_Terminal">aseq</span> and <span class="Code_Terminal">cseq</span> indicate the number of operations loaded</li>
        </ul>
        <p>The restored node must have the same bseq and aseq/cseq as the other nodes.</p>
    </body>
</html>