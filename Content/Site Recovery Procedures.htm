<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd">
    <head><title>Site Recovery Procedures</title>
        <link href="Resources/TableStyles/SimpleDefinitions.css" rel="stylesheet" MadCap:stylesheetType="table" />
    </head>
    <body>
        
        <h1 MadCap:autonum="1. &#160;">Site Recovery Procedures</h1>
        <p style="text-align: left;">In a multi-site deployment where one or more sites are lost, site recovery procedures are available to help regain the Raft session quorum  (or increase the number of available nodes to cover in a situation where more could be down).</p>
        <p>S3 Connector supports multi-site deployments for metro-area networks with low-latency network connections between the sites (connected within 10 ms of round-trip network latency). These are termed <i>stretched</i> deployments in that they leverage a single logical copy of the Scality RING, with synchronous updates on the machines deployed in the data centers that comprise the deployment. S3 Connector supports two-site (and also three-site) stretched deployments, with the benefit that if one data center is lost or becomes temporarily inaccessible, the administrator can use the recovery mechanisms described in this chapter to restore service availability from the remaining site(s). Full details on installing and operating two-site and three-site stretched S3 Connector deployments are provided in the <span class="PublicationName"><MadCap:variable name="General.SetupInstall" /></span>.</p>
        <p>The main requirement for recovering the service is to have previously deployed warm standby servers (WSBs). A warm standby is a Metadata node substitute, also called a <i>shadow</i> node that stays up to date by shadowing one active node of the current Raft session (using the backups stored in the RING). </p>
        <p style="text-align: left;">For more information about the supported S3 Connector stretched deployment models, refer to the <span class="PublicationName"><MadCap:variable name="General.SetupInstall" /></span>.</p>
        <h2 MadCap:autonum="1.1. &#160;">Recovering a Two-Site, Six-Server Deployment</h2>
        <p>In a two-site deployment, when one site is lost,  a few manual operations must be performed to resume service. Once the lost site comes up again, it will first have to catch up and synchronize itself with the active site. Once both sites are in the same state, the service is briefly interrupted again to return to a nominal dual-site state.</p>
        <table style="width: 100%;mc-table-style: url('Resources/TableStyles/SimpleDefinitions.css');" class="TableStyle-SimpleDefinitions" cellspacing="0">
            <col class="TableStyle-SimpleDefinitions-Column-Column1" style="width: 117px;" />
            <col class="TableStyle-SimpleDefinitions-Column-Column1" style="width: 471px;" />
            <tbody>
                <tr class="TableStyle-SimpleDefinitions-Body-Row1">
                    <td class="TableStyle-SimpleDefinitions-BodyE-Column1-Row1">
                        <p><i>majority</i> site</p>
                    </td>
                    <td class="TableStyle-SimpleDefinitions-BodyD-Column1-Row1">
                        <p class="InTableFormatted">Three servers are active metadata nodes.</p>
                    </td>
                </tr>
                <tr class="TableStyle-SimpleDefinitions-Body-Row1">
                    <td class="TableStyle-SimpleDefinitions-BodyB-Column1-Row1">
                        <p><i>minority</i> site</p>
                    </td>
                    <td class="TableStyle-SimpleDefinitions-BodyA-Column1-Row1">
                        <p class="InTableFormatted">Two servers are active metadata nodes, and one is a WSB shadowing one of the servers on the majority site.</p>
                    </td>
                </tr>
            </tbody>
        </table>
        <p style="text-align: left;page-break-after: avoid;">The playbooks for the procedure use Ansible options:</p>
        <table style="mc-table-style: url('Resources/TableStyles/SimpleDefinitions.css'); width: 100%;" class="TableStyle-SimpleDefinitions" cellspacing="0">
            <col class="TableStyle-SimpleDefinitions-Column-Column1" style="width: 117px;" />
            <col class="TableStyle-SimpleDefinitions-Column-Column1" style="width: 471px;" />
            <tbody>
                <tr class="TableStyle-SimpleDefinitions-Body-Row1">
                    <td class="TableStyle-SimpleDefinitions-BodyE-Column1-Row1" style="font-size: 9pt;text-align: left;"><pre>-l</pre>
                    </td>
                    <td class="TableStyle-SimpleDefinitions-BodyD-Column1-Row1" style="font-size: 9pt;text-align: left;">
                        <p>This option allows playbooks to be run on only certain hosts. If it is not invoked, the playbook fails with unreachable hosts.</p>
                    </td>
                </tr>
                <tr class="TableStyle-SimpleDefinitions-Body-Row1">
                    <td class="TableStyle-SimpleDefinitions-BodyB-Column1-Row1" style="font-size: 9pt;text-align: left;"><pre>-e</pre>
                    </td>
                    <td class="TableStyle-SimpleDefinitions-BodyA-Column1-Row1" style="font-size: 9pt;text-align: left;">This option allows custom parameters to be provided to the playbooks (overriding their values in the inventory). Two main custom parameters are used,  <code>lost_site</code> (the group representing the lost site) and <code>env_metadata_force_leader_on_site</code> (forces the leader of a Raft session on a given site).
                    </td>
                </tr>
            </tbody>
        </table>
        <h3 MadCap:autonum="1.1.1 &#160;">Minority Site Loss</h3>
        <p>In a two-site, six-server deployment, if the minority site is lost, the service becomes temporarily unavailable as the Raft session leader is forced on that site. Allowing the leader to temporarily be on the majority site resumes service. Once the minority site is back up, it must catch up with the majority site. After monitoring this catch-up, service is temporarily disabled while returning to nominal state.</p>
        <p class="BoxIMPORTANT" style="text-align: left;">A site loss is usually associated with power outage, fire, or another unpredictable event. If no machine on the minority site answers to ping, the site has been lost.<br /><br />If servers on the minority site can be reached, but there is trouble with the S3 service, do not follow the site loss procedure.</p>
        <h4>Step 1: Allow the Leader to Be on the Majority Site</h4>
        <p style="text-align: left;">Allowing the leader to be on the majority site resumes the service. This is done through the <i>run.yml</i> playbook, using the <i>DR</i> tag (thus running only certain steps associated with that tag). The changes are applied only to the majority site servers.</p><pre xml:space="preserve">$ ansible-playbook -i env/{{ ENVDIR }}/inventory run.yml -t DR -l majority -e "env_metadata_force_leader_on_site=majority" -s</pre>
        <h4>Step 2: Confirm Read/Write Access</h4>
        <p>Try listing  or putting an object to a bucket. If this is not possible, check if that bucket is associated with a Raft leader:</p><pre>$ curl http://{{ MAJORITYSERVERIP }}:9000/default/leader/{{ BUCKETNAME }}</pre>
        <h4>Step 3: Return of the Lost Site</h4>
        <p>The procedure cannot be continued until the minority site returns.</p>
        <h4>Step 4: Force Warm-Standby Behavior on All Minority Servers</h4>
        <p>Temporarily force WSB behavior on all minority servers to catch them up with the majority Raft session:</p><pre xml:space="preserve">$ ansible-playbook -i env/{{ ENVDIR }}/inventory -l minority tooling-playbooks/force_warm_stand_by.yml -s  -e 'force_warm=minority'</pre>
        <p class="BoxNote">In a decoupled setup, augment the value given to Ansible’s
<span class="Code_Terminal">-l</span> option (a comma-separated list of hosts or group names) with the groups of still-reachable stateless connectors. The services directly accessing Metadata may require a configuration update.</p>
        <h4>Step 5: Wait for the Minority Site to Catch Up with the RING </h4>
        <p>A playbook is provided to monitor catch-up. Repeat this step until the minority and majority values (number of batch backups to the RING) are the same.</p><pre>$ ansible-playbook -i ./env/{{ ENVDIR }}/inventory tooling-playbooks/check-warmstandby-recover.yml -e 'recovering="minority" active="majority"' -s</pre>
        <h4>Step 6: Resume Active Nodes on the Minority Site</h4>
        <p>Once the values are the same on both sites, WSB behavior no longer must be forced on all minority machines. Having two Raft session nodes once again active on the minority site enables more granular catch-up with the Raft session leader.</p><pre xml:space="preserve">$ ansible-playbook -i env/{{ ENVDIR }}/inventory tooling-playbooks/return_to_normal.yml -s</pre>
        <h4>Step 7: Wait for Minority Active Nodes to Catch Up with the Leader</h4>
        <p>Run the provided catch-up monitoring playbook. Repeat this step until the playbook concludes that the minority and majority values (the Raft counter for Metadata operations) are the same.</p><pre>$ ansible-playbook -i ./env/{{ ENVDIR }}/inventory tooling-playbooks/check-warmstandby-leader-switch.yml</pre>
        <p class="BoxTip">Once the values are the same on both sites, the service must be stopped while returning to nominal state (stop all the S3 containers). Once this is done, repeat the previous step to ensure that all operations are known to both sites.</p><pre>$ ansible -i ./env/{{ ENVDIR }}/inventory all -m shell -a 'docker stop scality-s3' -s</pre>
        <p class="BoxTip">If this step fails (due to Ansible not being activated in the vitualenv) activate it with:&#160;
				<pre style="text-align: left;" xml:space="preserve">$ ./path/to/s3-offline-VERSION/repo/venv/bin/activate
$ ./path/to/s3-offline-VERSION/repo/venv/bin/ansible</pre></p><pre>$ ansible-playbook -i ./env/{{ ENVDIR }}/inventory tooling-playbooks/ check-warmstandby-leader-switch.yml</pre>
        <h4>Step 8: Return to Nominal State</h4>
        <p>The leader must eventually be forced back onto the minority site to cover for a potential upcoming majority site loss. To do this, run the <span class="ProprietaryFileName">run.yml</span> playbook with the <span class="ElementName">DR</span> tag.</p><pre xml:space="preserve">$ ansible-playbook -i ./env/testing/inventory run.yml -t DR -s<br />$ ansible -i ./env/{{ ENVDIR }}/inventory all -m shell -a 'docker start scality-s3' -s</pre>
        <p class="BoxIMPORTANT"> The service must be resumed upon completion.</p>
        <h3 id="Majority Site Loss" MadCap:autonum="1.1.2 &#160;">Majority Site Loss</h3>
        <p>In a two-site, six-server deployment, if the majority site is lost, the service becomes temporarily unavailable as the Raft session has only two active nodes on the minority site. Temporarily activating the WSB on the minority site resumes service. Once the majority site is back up, all servers must be forced into WSB mode until they catch up with the minority site, and this catch-up must be monitored. When resuming to nominal state, there is a short service loss as the minority site WSB resumes that function, and the majority site nodes become active nodes again.</p>
        <p class="BoxIMPORTANT" style="text-align: left;">A site loss is usually associated with power outage, fire, or other unpredictible event. If no machine on the majority site answers to ping, the site is lost.<br /><br />If the majority servers answer to ping, but the S3 service is not behaving as expected, do not follow this procedure.</p>
        <h4>Step 1: Make the Warm Standby Server Active</h4>
        <p>Making the minority warm standby an active node resumes the service. A specific playbook exists for this purpose: it must be told which site is lost, and on which site the warm standby shall be activated.</p><pre xml:space="preserve">$ ansible-playbook -i env/{{ ENVDIR }}/inventory -l minority -e 'lost_sites="majority"' tooling-playbooks/activate_warm_stand_by.yml -s</pre>
        <h4>Step 2: Confirm Read/Write Access</h4>
        <p>Try listing a bucket or putting an object to one. If this fails, check if that bucket is associated with a Raft leader:</p><pre xml:space="preserve">$&#160;curl http://{{ MINORITYSERVERIP }}:9000/default/leader/{{ BUCKETNAME }}</pre>
        <h4>Step 3: Return of Lost Site</h4>
        <p>The procedure cannot be continued until the minority site returns.</p>
        <h4>Step 4: Force Warm Standby Behavior on All Majority Servers</h4>
        <p>For minority servers to catch up with the minority Raft session, temporarily force all minority servers to warm standby:</p><pre xml:space="preserve">$ ansible-playbook -i env/{{ ENVDIR }}/inventory -l majority tooling-playbooks/force_warm_stand_by.yml -s -e 'force_warm=majority'</pre>
        <p class="BoxNote">In a decoupled setup, augment the value given to Ansible’s
<span class="Code_Terminal">-l</span> option 			(a comma-separated list of hosts or group names) 			 with the groups of still-reachable stateless connectors. The services directly accessing Metadata may
		require a configuration update.</p>
        <h4>Step 5: Wait for the Minority Site to Catch Up with the RING</h4>
        <p>A playbook is provided to monitor catch-up. Repeat this step until the minority and majority values (number of batch backups to the RING) are the same.</p><pre xml:space="preserve">$ ansible-playbook -i ./env/{{ ENVDIR }}/inventory tooling-playbooks/check-warmstandby-recover.yml -e 'recovering="majority" active="minority"' -s</pre>
        <h4>Step 6: Return to Nominal State</h4>
        <p>Once the values are the same on both sites, the majority machines can resume as active nodes. Returning to having three active nodes on the majority site with one warm standby on the minority site incurs a short downtime while the metadata containers restart with their new config.</p><pre xml:space="preserve">$ ansible-playbook -i env/{{ ENVDIR }}/inventory tooling-playbooks/return_to_normal.yml -s</pre>
    </body>
</html>
