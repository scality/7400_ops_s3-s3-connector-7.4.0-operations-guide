﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd">
    <head>
    </head>
    <body>
        <h1 MadCap:autonum="1. &#160;">Bucket-Level&#160;CRR</h1>
        <p>This section contains steps to set up cross-region replication using the Backbeat feature. </p>
        <h2 MadCap:autonum="1.1. &#160;">Setting Up Bucket-Level Cross-Region Replication</h2>
        <p>S3 Connector offers bucket-level cross-region replication  (CRR).</p>
        <p>Using Federation to install S3 Connector installs all prerequisites needed to run a CRR&#160;installation. Deploying CRR&#160;is simply a matter of configuration, and of having primary and secondary (failover)&#160;sites operational.  </p>
        <h4>Configuration</h4>
        <p>To enable CRR, uncomment the <code>env_replication_endpoints:</code> section in the <span class="ProprietaryFileName">group_vars/all </span>file, add your destination locations (including the servers and ports used at each destination) and deactivate echo mode for each destination. Go to Federation’s Backbeat configuration (in the <span class="ElementName">env_replication_ endpoints</span> section of <span class="ProprietaryFileName">env/client-template/group_vars/all</span>) and set <code>echo: false</code> before deployment.</p>
        <p class="BoxIMPORTANT" style="text-align: left;">Use the same Vault administrative credentials on both the source and target sites (<code>env/client-template/ adminclientprofile/admin1.json</code>). Backbeat requires the same credentials to create accounts on source and target.</p>
        <p style="page-break-after: avoid;">The relevant section of <span class="ProprietaryFileName">group_vars/all</span> is:</p><pre xml:space="preserve"># Configuration block for Backbeat's Cross Region Replication
# Define target endpoints for async replication. It takes a list of sites with
# the site property to identify a site uniquely and a list of servers where each
# item is &lt;host&gt;:&lt;port&gt; where host is a dns name or an ip address of the
# load balancer or nginx frontend and port is the
# env_backbeat_replication_port defined at the target site installation.
# The property `echo` controls site level replication. If set to true, any
# bucket created on the source will be automatically replicated to a bucket with
# the same name on target. The default is false.
# When defining more than one replication endpoint, it is required to pick one
# of them as a default. Any replication requests specified without an endpoint
# will use the default endpoint as the target.
# Note: Site property can be alphanumeric and the only special characters
# allowed are `-` and `_`
#
# env_replication_endpoints:
#   - site: paris
#     default: true
#     servers:
#       - paris1.example.com:9080
#       - paris2.example.com:9080
#       - paris3.example.com:9080
#       - paris4.example.com:9080
#       - paris5.example.com:9080
#     echo: false</pre>
        <h2 MadCap:autonum="1.2. &#160;">Example:&#160;Using Internal Tooling</h2>
        <p>  This example demonstrates how to set up cross-region replication (CRR) between two sites:</p>
        <ul>
            <li><b>sf</b> (source)</li>
            <li><b>paris</b> (target)</li>
        </ul>
        <ol MadCap:continue="false">
            <li>
                <p>Generate account credentials for each site using the command:</p><pre xml:space="preserve">$&#160;ansible-playbook -i env/client-template/inventory tooling-playbooks/generate-account-access-key.yml -e 'ui_username={{ui_username}} ui_password={{ui_password}}'</pre>
            </li>
            <li>
                <p>At the source, log in to the Docker shell of a Backbeat container:</p><pre xml:space="preserve">$ docker exec -it -u scality scality-backbeat-1 bash</pre>
            </li>
            <li>
                <p>Store the account credentials:</p><pre>$ mkdir ~/.aws
$&#160;vi ~/.aws/credentials</pre>
                <p style="page-break-after: avoid;">The completed file should resemble the following:</p><pre>[sf]
aws_access_key_id = DLJ0S4DGZF2MPY6A6LL5
aws_secret_access_key = M94dfHOFoqTG+nWtvMpZp=seca4=rPft6q6/Wy02
[paris]
aws_access_key_id = 7YR5G1L0L1RZ3382VIPT
aws_secret_access_key = =QycZk0j=nyt1Ju+hBYYKb3da=ER3MdslIUknhpw</pre>
            </li>
            <li>
                <p style="page-break-after: avoid;">Set up replication on your buckets:</p><pre xml:space="preserve">$ node ~/backbeat/bin/replication.js setup --source-bucket source-bucket --source-profile sf --target-bucket target-bucket --target-profile paris</pre>
                <p>We are now ready to put data on the source bucket and watch it replicate to the target bucket.</p>
            </li>
            <li>
                <p>Put an object on the source bucket:</p><pre xml:space="preserve">$ echo 'content to be replicated' &gt; replication_contents &amp;&amp; aws s3api put-object --bucket source-bucket --key object-to-replicate --body replication_contents --endpoint http://127.0.0.1 --profile sf</pre>
            </li>
            <li>
                <p>Check the replication status of the just-put object:</p><pre xml:space="preserve">$ aws s3api head-object --bucket source-bucket --key object-to-replicate --endpoint http://&lt;source&gt;:{{publicNginxPort}}</pre>
                <p>The object’s <span class="state">ReplicationStatus</span> should either be <span class="state">PENDING </span>or <span class="state">COMPLETED</span>, depending on how long replication takes. </p>
            </li>
            <li>
                <p>
Verify that the object has been replicated to the target bucket: </p><pre xml:space="preserve">$ aws s3api head-object --bucket target-bucket --key object-to-replicate --endpoint http://&lt;target&gt;:{{publicNginxPort}}</pre>
                <p>After some time, the object’s <span class="state">ReplicationStatus </span>should be <span class="state">REPLICA</span>.</p>
            </li>
        </ol>
        <h2 MadCap:autonum="1.3. &#160;">Example: Using the API</h2>
        <p style="page-break-after: avoid;">In this example:
</p>
        <ul>
            <li><code>node1</code> is a machine from the source environment.</li>
            <li><code>node6</code> is a machine from the destination environment.</li>
            <li><code>PFW10LROZVQ8B0FZ8XCO</code> and <code>ATe+CSJuM7hFwAvhk3JeUZ+Cfc09te+Q7tUOvNZE</code> are access and secret keys on the source environment.</li>
            <li><code>EK4Y73PVLCPPB88I0ES1</code> and <code>KoU2SpIlqkbshXYyV3/O/jO6VmWkbmnF64o+DbM4</code>  are access and secret keys on the destination environment.</li>
            <li><code>sourcebucket</code> is the CRR bucket on the source environment.</li>
            <li><code>destinationbucket</code> is the CRR bucket on the destination environment.</li>
        </ul>
        <p>Adjust these variables to match your environment.

</p>
        <p>The commands in the procedure below do not all have to be run from the same machine. It is possible to run the commands for the source environment on a machine located within the source environment, and to run the commands for the destination environment on a machine located in the destination environment.</p>
        <ol>
            <li>
                <p>

Create a bucket on the source and a bucket on the destination:
</p><pre xml:space="preserve">$ s3cmd -c /home/scality/.s3cfg.source mb s3://sourcebucket</pre><pre xml:space="preserve">$ s3cmd -c /home/scality/.s3cfg.destination mb s3://destinationbucket
</pre>
            </li>
            <li>
                <p>Enable bucket versioning on each bucket:
</p><pre xml:space="preserve">
AWS_ACCESS_KEY_ID=PFW10LROZVQ8B0FZ8XCO AWS_SECRET_ACCESS_KEY=ATe+CSJuM7hFwAvhk3JeUZ+Cfc09te+Q7tUOvNZE aws --endpoint http://node1 s3api put-bucket-versioning --bucket sourcebucket --versioning-configuration Status=Enabled
AWS_ACCESS_KEY_ID=EK4Y73PVLCPPB88I0ES1 AWS_SECRET_ACCESS_KEY=KoU2SpIlqkbshXYyV3/O/jO6VmWkbmnF64o+DbM4 aws --endpoint http://node6 s3api put-bucket-versioning --bucket destinationbucket --versioning-configuration Status=Enabled</pre>
                <p>
To verify that bucket versioning is enabled on a bucket, you can use:
</p><pre xml:space="preserve">AWS_ACCESS_KEY_ID=PFW10LROZVQ8B0FZ8XCO AWS_SECRET_ACCESS_KEY=ATe+CSJuM7hFwAvhk3JeUZ+Cfc09te+Q7tUOvNZE aws --endpoint http://node1 s3api get-bucket-versioning --bucket sourcebucket
AWS_ACCESS_KEY_ID=EK4Y73PVLCPPB88I0ES1 AWS_SECRET_ACCESS_KEY=KoU2SpIlqkbshXYyV3/O/jO6VmWkbmnF64o+DbM4 aws --endpoint http://node6 s3api get-bucket-versioning --bucket destinationbucket  </pre>
                <p>We now need to establish a bucket policy to allow the replication engine (backbeat) to retrieve the appropriate information.
</p>
            </li>
            <li>
                <p>Create the file <span class="FileName">dest.policy.json </span>with the following content:</p><pre xml:space="preserve">
{
   "Version":"2012-10-17",
   "Statement":[
      {
         "Effect":"Allow",
         "Action":[
            "s3:GetObjectVersion",
            "s3:GetObjectVersionAcl"
         ],
         "Resource":[
            "arn:aws:s3:::sourcebucket/*"
         ]
      },
      {
         "Effect":"Allow",
         "Action":[
            "s3:ListBucket",
            "s3:GetReplicationConfiguration"
         ],
         "Resource":[
            "arn:aws:s3:::sourcebucket"
         ]
      },
      {
         "Effect":"Allow",
         "Action":[
            "s3:ReplicateObject",
            "s3:ReplicateDelete"
         ],
         "Resource":"arn:aws:s3:::destinationbucket/*"
      }
   ]
}</pre>
            </li>
            <li>
                <p xml:space="preserve">
Create the above policy on the source and destination IAM environments:</p><pre xml:space="preserve">
AWS_ACCESS_KEY_ID=PFW10LROZVQ8B0FZ8XCO AWS_SECRET_ACCESS_KEY=ATe+CSJuM7hFwAvhk3JeUZ+Cfc09te+Q7tUOvNZE aws --endpoint http://node1:8600 iam create-policy --policy-name my-policy --policy-document file://dest.policy.json
{
    "Policy": {
        "PolicyName": "my-policy",
        "CreateDate": "2017-07-21T18:44:01Z",
        "AttachmentCount": 0,
        "IsAttachable": true,
        "PolicyId": "R8W4K5XV2XDF4ZLH0NW9ZPCZDEI4H5CK",
        "DefaultVersionId": "v1",
        "Path": "/",
        "Arn": "arn:aws:iam::316133440783:policy/my-policy",
        "UpdateDate": "2017-07-21T18:44:01Z"
    }
}

AWS_ACCESS_KEY_ID=EK4Y73PVLCPPB88I0ES1 AWS_SECRET_ACCESS_KEY=KoU2SpIlqkbshXYyV3/O/jO6VmWkbmnF64o+DbM4  aws --endpoint http://node6:8600 iam create-policy --policy-name my-policy --policy-document file://dest.policy.json

{
    "Policy": {
        "PolicyName": "my-policy",
        "CreateDate": "2017-07-21T18:43:35Z",
        "AttachmentCount": 0,
        "IsAttachable": true,
        "PolicyId": "3J9TNV56AZ3RB3W9XFUW31XUPOGZM55H",
        "DefaultVersionId": "v1",
        "Path": "/",
        "Arn": "arn:aws:iam::402568422749:policy/my-policy",
        "UpdateDate": "2017-07-21T18:43:35Z"
    }
}
</pre>
                <p xml:space="preserve">
We then need to create the roles for the Backbeat replication engine to use. </p>
            </li>
            <li>
                <p>To establish the roles for the Backbeat replication engine, create <span class="ProprietaryFileName">trust.json</span> with the following content:


</p><pre xml:space="preserve">
{
   "Version":"2012-10-17",
   "Statement":[
      {
         "Effect":"Allow",
         "Principal":{
            "Service":"backbeat"
         },
         "Action":"sts:AssumeRole"
      }
   ]
}</pre>
            </li>
            <li>
                <p>

Create roles on the source and destination environments:</p><pre xml:space="preserve">
AWS_ACCESS_KEY_ID=PFW10LROZVQ8B0FZ8XCO AWS_SECRET_ACCESS_KEY=ATe+CSJuM7hFwAvhk3JeUZ+Cfc09te+Q7tUOvNZE aws --endpoint http://node1:8600 iam create-role --role-name Test-Role --assume-role-policy-document file://trust.json

{
    "Role": {
        "AssumeRolePolicyDocument": {
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Action": "sts:AssumeRole",
                    "Effect": "Allow",
                    "Principal": {
                        "Service": "backbeat"
                    }
                }
            ]
        },
        "RoleId": "ZX6QI5K88ISYCNMIDZVJTBW5QE101DLN",
        "CreateDate": "2017-07-21T18:46:31Z",
        "RoleName": "Test-Role",
        "Path": "/",
        "Arn": "arn:aws:iam::316133440783:role/Test-Role"
    }
}

AWS_ACCESS_KEY_ID=EK4Y73PVLCPPB88I0ES1 AWS_SECRET_ACCESS_KEY=KoU2SpIlqkbshXYyV3/O/jO6VmWkbmnF64o+DbM4  aws --endpoint http://node6:8600 iam create-role --role-name Test-Role --assume-role-policy-document file://trust.json

{
    "Role": {
        "AssumeRolePolicyDocument": {
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Action": "sts:AssumeRole",
                    "Effect": "Allow",
                    "Principal": {
                        "Service": "backbeat"
                    }
                }
            ]
        },
        "RoleId": "MNSHH2H0JE60VRQ67HF6KJN85OEJ70Z7",
        "CreateDate": "2017-07-21T18:45:03Z",
        "RoleName": "Test-Role",
        "Path": "/",
        "Arn": "arn:aws:iam::402568422749:role/Test-Role"
    }
}
</pre>
            </li>
            <li>
                <p xml:space="preserve">

Attach the created policy to the roles:</p><pre xml:space="preserve">
AWS_ACCESS_KEY_ID=PFW10LROZVQ8B0FZ8XCO AWS_SECRET_ACCESS_KEY=ATe+CSJuM7hFwAvhk3JeUZ+Cfc09te+Q7tUOvNZE aws --endpoint http://node1:8600 iam attach-role-policy --policy-arn arn:aws:iam::316133440783:policy/my-policy --role-name Test-Role

AWS_ACCESS_KEY_ID=EK4Y73PVLCPPB88I0ES1 AWS_SECRET_ACCESS_KEY=KoU2SpIlqkbshXYyV3/O/jO6VmWkbmnF64o+DbM4  aws --endpoint http://node6:8600 iam attach-role-policy --policy-arn arn:aws:iam::402568422749:policy/my-policy  --role-name Test-Role
</pre>
                <p xml:space="preserve">The <code>arn</code> must reflect the <code>arn</code> of the policies created with create-policy.
</p>
            </li>
            <li>
                <p>Create the file <span class="ProprietaryFileName">replication.json</span> with the following content:
</p><pre xml:space="preserve">
{
  "Role": "arn:aws:iam::316133440783:role/Test-Role,arn:aws:iam::402568422749:role/Test-Role",
  "Rules": [
    {
      "Prefix": "",
      "Status": "Enabled",
      "Destination": {
        "Bucket": "arn:aws:s3:::destinationbucket"
      }
    }
  ]
}</pre>
            </li>
            <li>
                <p xml:space="preserve">

On the source environment, enable replication:</p><pre xml:space="preserve">
AWS_ACCESS_KEY_ID=PFW10LROZVQ8B0FZ8XCO AWS_SECRET_ACCESS_KEY=ATe+CSJuM7hFwAvhk3JeUZ+Cfc09te+Q7tUOvNZE aws --endpoint http://node1 s3api put-bucket-replication --bucket sourcebucket --replication-configuration file://replication.json </pre>
            </li>
        </ol>
    </body>
</html>