﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd">
    <head>
    </head>
    <body>
        <h2 MadCap:autonum="1.1. &#160;"><a name="top"></a>Site-Level CRR</h2>
        <p>Site-level CRR&#160;replicates the contents of an entire site in near-real-time to a bucket located remotely (outside the region).</p>
        <h3 MadCap:autonum="1.1.1 &#160;">Workflow</h3>
        <p>When a bucket is created on the source, the Queue Populator creates a bucket-creation  entry in Backbeat for the next batch process (normally about every 5 seconds).</p>
        <p>If site-level replication mode is enabled, the CRR Queue Processor fetches the bucket creation entry and does the following to replicate the bucket on the target and enable CRR between the buckets:</p>
        <ol>
            <li>The Queue Processor retrieves source account attributes (name, email) from Vault using the configured Vault administrative credentials.</li>
            <li>If the source bucket owner’s name and email do not already exist on the target, the Queue Processor creates a new account there with the same name and email as the source bucket owner, using the administrative credentials configured for Vault.</li>
            <li>The Queue Processor creates a new access/secret key pair for the source account and for the target account. The first time, Backbeat needs access to these accounts after it starts. Afterwards, Backbeat uses the cached credentials (credentials are kept in process memory but not stored on disk).</li>
            <li>
                <p>If the bucket does not exist on the target site, the Queue Processor creates it.</p>
            </li>
            <li>The Queue Processor creates new roles on the source and destination to support CRR actions through Backbeat.</li>
            <li>The Queue Processor creates new  policies on the source and destination to allow CRR to execute the following actions on behalf of the user:<ul><li>ListBucket, GetReplicationConfiguration on the source bucket</li><li>GetObjectVersion, GetObjectVersionAcl on that bucket’s source objects</li><li>ReplicateObject, ReplicateDelete on the destination bucket’s target objects</li></ul></li>
            <li>The Queue Processor attaches the new policies to their respective new roles on the source and the target.</li>
            <li>The Queue Processor enables versioning on the source and destination buckets.</li>
            <li>The Queue Processor enables replication on the source bucket.</li>
        </ol>
        <p>From this point on, every object created on the source has a new version that will be replicated on the target, using the role and temporary credentials as for normal CRR mode.</p>
        <p class="BoxNote">All the above steps must be executed before new objects written to the source bucket are automatically replicated. To know when CRR is enabled for new objects written to the source bucket, poll the source bucket for its replication configuration (for example, with <code>aws CLI aws s3api get-bucket-replication</code>) until it returns one.</p>
        <h3 MadCap:autonum="1.1.2 &#160;"><a name="Setting2"></a>Setting Up Site-Level Cross-Region Replication</h3>
        <p>S3 Connector offers site-level cross-region replication  (CRR). Site-level CRR enables a simplified replication setup that facilitates disaster recovery.</p>
        <p>When enabled, site-level replication triggers the automatic configuration of any newly-created bucket on the source for CRR, freeing the operator from having to set up replication, roles, and policies manually or through a helper script. Bucket contents are replicated to remote sites in a bucket of the same name. Bucket owner names are also replicated, but because the source and destination maintain discrete identity management facilities, the owners are functionally discrete entities, with different accounts and access keys.</p>
        <p>Using Federation to install S3 Connector installs all prerequisites needed to run a CRR&#160;installation. CRR&#160;is simply a matter of configuration, and of having primary and secondary (failover)&#160;sites operational.  </p>
        <h4>Configuration</h4>
        <p>To enable CRR, uncomment the <code>env_replication_endpoints:</code> section in the <span class="ProprietaryFileName">group_vars/all </span>file, add your destination locations (including the servers and ports used at each destination) and activate echo mode for each destination. Go to Federation’s Backbeat configuration (in the <span class="ElementName">env_replication_ endpoints</span> section of <span class="ProprietaryFileName">env/client-template/group_vars/all</span>) and set <code>echo: true</code> before deployment.</p>
        <p class="BoxIMPORTANT" style="text-align: left;">Use the same Vault administrative credentials on both the source and target sites (<span class="Code_Terminal">env/client-template/admin-clientprofile/admin1.json</span>). Backbeat requires the same credentials to create accounts on source and target.</p>
        <p style="page-break-after: avoid;">The relevant section of <span class="ProprietaryFileName">group_vars/all</span> is as follows:</p><pre xml:space="preserve"># Configuration block for Backbeat's Cross Region Replication
# Define target endpoints for async replication. It takes a list of sites with
# the site property to identify a site uniquely and a list of servers where each
# item is &lt;host&gt;:&lt;port&gt; where host is a dns name or an ip address of the
# load balancer or nginx frontend and port is the
# env_backbeat_replication_port defined at the target site installation.
# The property `echo` controls site level replication. If set to true, any
# bucket created on the source will be automatically replicated to a bucket with
# the same name on target. The default is false.
# When defining more than one replication endpoint, it is required to pick one
# of them as a default. Any replication requests specified without an endpoint
# will use the default endpoint as the target.
# Note: Site property can be alphanumeric and the only special characters
# allowed are `-` and `_`
#
# env_replication_endpoints:
#   - site: paris
#     default: true
#     servers:
#       - paris1.example.com:9080
#       - paris2.example.com:9080
#       - paris3.example.com:9080
#       - paris4.example.com:9080
#       - paris5.example.com:9080
#     echo: false</pre>
        <h3 MadCap:conditions="PrintGuides.Zenko" MadCap:autonum="1.1.3 &#160;">Failover and Failback Scenarios</h3>
        <p MadCap:conditions="PrintGuides.Zenko">If site-level replication mode is enabled on the source, you can also enable it also on the target, pointing to the source as CRR target. This enables automatic replication of objects written on the secondary (failover)&#160;site to the primary site. In failover scenarios, after the secondary site becomes active and receives writes, all updates are replicated to the primary site on its recovery.</p>
        <p MadCap:conditions="PrintGuides.Zenko">To avoid accumulating Kafka updates during failover and eventually losing them to either pruning or loss of disk space, stop the Backbeat container on the secondary site until the primary site is back up. This ensures that updates can be timely pushed to Kafka and processed.</p>
        <p MadCap:conditions="PrintGuides.Zenko">After the secondary site starts Backbeat, when Backbeat CRR processes a bucket creation entry to sync to the primary site, this entry goes through the same processing as normal CRR to the secondary site. In other words, replication is enabled on that bucket, and new roles and policies are created and attached together. Each bucket becomes accessible to both primary- and secondary-site Backbeat processes for their respective CRR actions. No new bucket is created on the source, which already exists, but a new resource policy is attached to allow Backbeat on the DR site to write new object versions.</p>
        <h3 MadCap:conditions="PrintGuides.Zenko" MadCap:autonum="1.1.4 &#160;">Retry on CRR&#160;Failure </h3>
        <p MadCap:conditions="PrintGuides.Zenko">Cross-region replication operations are subject to failure. In earlier releases, large CRR&#160;operations did not fail gracefully:&#160;failed operations could require repetition of an entire CRR&#160;operation. With <MadCap:variable name="General.VersionNumber"></MadCap:variable>, <MadCap:variable name="General.ProductNameShort"></MadCap:variable> provides a way to monitor CRR&#160;operations, retrieve a list of failed operations, and retry specific operations on failure.</p>
        <p MadCap:conditions="PrintGuides.Zenko">Access to CRR&#160;Retry is currently by way of a RESTful&#160;API&#160;exposed through Backbeat. See <span class="PublicationName"><MadCap:variable name="General.ReferenceManual" /></span> for more detail on the Backbeat API.</p>
    </body>
</html>